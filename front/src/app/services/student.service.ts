import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Student} from '../model/student.model';

@Injectable()
export class StudentService {

  constructor(private http: HttpClient) {}

  getAll(): Observable<Student[]> {
    return this.http.get<Student[]>('/api/students');
  }

  getById(id: number): Observable<Student> {
    return this.http.get<Student>(`/api/students/${id}`);
  }

  updateCourses(id: number, courses: string[]): Observable<Student> {
    return this.http.put<Student>(`/api/students/${id}`, courses);
  }

}
