import {Component, EventEmitter, OnInit, Output, Input} from '@angular/core';
import {StudentService} from '../../services/student.service';
import {Student} from '../../model/student.model';

@Component({
  selector: 'app-student-card',
  templateUrl: './student-card.component.html',
  styleUrls: ['./student-card.component.css']
})
export class StudentCardComponent implements OnInit {

  @Input() id: number;
  @Output() close = new EventEmitter<boolean>();
  private student: Student;
  private courses: Course[];

  constructor(private studentService: StudentService) { }

  ngOnInit() {
    this.studentService.getById(this.id).subscribe(data => {
      this.student = data;
      this.courses = data.courses.map(name => new Course(name));
    });
  }

  addRow() {
    this.courses.push(new Course(''));
  }

  deleteRow(row: Course) {
    this.courses.splice(this.courses.indexOf(row), 1);
  }

  save() {
    this.studentService.updateCourses(this.id, this.courses.map(course => course.name))
      .subscribe(data => this.close.emit(true));
  }
  cancel() {
    this.close.emit(false);
  }
}

class Course {
  name: string;

  constructor(name: string) {
    this.name = name;
  }
}
