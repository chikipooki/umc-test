package kz.umc.student.service;

import kz.umc.student.model.Student;
import kz.umc.student.rest.dto.StudentCardDTO;
import kz.umc.student.rest.dto.StudentDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dastan on 4/23/18.
 */
@Service
@Transactional
@AllArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;


    public List<StudentDTO> getAll() {
        return studentRepository.findAll()
                .stream()
                .map(StudentDTO::new)
                .collect(Collectors.toList());
    }

    public StudentCardDTO get(Long id) {
        Student student = studentRepository.findOne(id);
        student.getCourses().size();
        return new StudentCardDTO(student, student.getCourses());
    }

    public StudentCardDTO update(Long id, List<String> courses) {
        Student student = studentRepository.findOne(id);
        student.setCourses(courses);
        student = studentRepository.save(student);
        return new StudentCardDTO(student, student.getCourses());
    }

}
