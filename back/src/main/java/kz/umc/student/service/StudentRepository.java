package kz.umc.student.service;

import kz.umc.student.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by dastan on 4/23/18.
 */
interface StudentRepository extends JpaRepository<Student, Long> {
}
