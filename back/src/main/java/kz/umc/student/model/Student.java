package kz.umc.student.model;

import lombok.Data;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by dastan on 4/23/18.
 */
@Data
@Entity
@Table(name = "students")
public class Student {
    @Id
    private Long id;

    private String firstname;

    private String lastname;

    @ElementCollection
    @JoinTable(name = "student_card", joinColumns = @JoinColumn(name = "student_id"))
    @Column(name = "course_name")
    private List<String> courses;
}
