package kz.umc.student.rest.dto;

import kz.umc.student.model.Student;

/**
 * Created by dastan on 4/23/18.
 */
public class StudentDTO {
    public Long id;
    public String firstname;
    public String lastname;

    public StudentDTO() {}

    public StudentDTO(Student student) {
        this.id = student.getId();
        this.firstname = student.getFirstname();
        this.lastname = student.getLastname();
    }
}
