package kz.umc.student.rest;

import kz.umc.student.rest.dto.StudentCardDTO;
import kz.umc.student.rest.dto.StudentDTO;
import kz.umc.student.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by dastan on 4/23/18.
 */
@RestController
@RequestMapping(produces = "application/json")
@AllArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping(value = "/students")
    public List<StudentDTO> getStudents() {
        return studentService.getAll();
    }

    @GetMapping(value = "/students/{id}")
    public StudentCardDTO getStudent(@PathVariable Long id) {
        return studentService.get(id);
    }

    @PutMapping(value = "/students/{id}")
    public StudentCardDTO updateCourses(@PathVariable Long id,
                                        @RequestBody List<String> newCourses) {
        return studentService.update(id, newCourses);
    }
}
