package kz.umc.student.rest.dto;

import kz.umc.student.model.Student;

import java.util.List;

/**
 * Created by dastan on 4/23/18.
 */
public class StudentCardDTO {
    public Long id;
    public String firstname;
    public String lastname;
    public List<String> courses;

    public StudentCardDTO(Student student, List<String> courses) {
        this.id = student.getId();
        this.firstname = student.getFirstname();
        this.lastname = student.getLastname();
        this.courses = courses;
    }
}
