CREATE TABLE `students` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`firstname` TEXT NOT NULL,
	`lastname` TEXT NOT NULL,
	PRIMARY KEY(`id`)
)
COLLATE='utf8_general_ci'
;

CREATE TABLE `student_card` (
	`student_id` INT NOT NULL,
	`course_name` TEXT NOT NULL,
	CONSTRAINT `student_fk` FOREIGN KEY(`student_id`) REFERENCES `students`(`id`)
)
COLLATE='utf8_general_ci'
;

INSERT INTO `students` (`id`, `firstname`, `lastname`) VALUES (1, 'Dastan', 'Yessekeyev');
INSERT INTO `students` (`id`, `firstname`, `lastname`) VALUES (2, 'Хайдар', 'Адиетов');
INSERT INTO `students` (`id`, `firstname`, `lastname`) VALUES (3, 'Ayim', 'Apayeva');

INSERT INTO `student_card` (`student_id`, `course_name`) VALUES (1, 'Algorithms and Data Structures');
INSERT INTO `student_card` (`student_id`, `course_name`) VALUES (2, 'Introduction into Machine Learning');
INSERT INTO `student_card` (`student_id`, `course_name`) VALUES (2, 'Advanced AI Algorithms');

