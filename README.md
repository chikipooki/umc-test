<h1>
Running instructions
</h1>
<h3>
cd front
</h3>
<h3>
npm install
</h3>
<h3>
npm start
</h3>
<h3>
cd ../back
</h3>
<h3>
mvn clean package
</h3>
<h3>
mvn spring-boot:run
</h3>
